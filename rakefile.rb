require "raykit"

VERSION = Raykit::Version::detect_from_file("conanfile.py", /version[\s]+=[\s]+"([\d\.]+)/, false)
task :env do
  start_task :env
  show_value "VERSION", "#{VERSION}"
end

task :clean do
  puts "  removing roam packages from the local cache"
  run("conan remove roam/* --force")
  run("git clean -dXf")
end

task :build => [:env] do
  start_task :build

  if (!PROJECT.read_only?)
    try("rufo .")
    #run("git clean -dXf")
    #puts `git clean -dXf`
    #run("conan remove roam/* --force")
  end

  if Gem.win_platform?
    run("conan config install https://gitlab.com/cpp-lib/roam.git -sf=profiles -tf=profiles")
    run("conan create . --profile win_vs2022_x64_static_release")
    Dir.chdir("test") do
      Dir.mkdir("build") if !Dir.exists?("build")
      #run "conan install . --install-folder build --build=missing --profile win_vs2022_x64_static_release"
      #PROJECT.run("conan install . --install-folder build --profile win_vs2022_x86_static_debug --build=missing")
      #PROJECT.run("conan install . --install-folder build --profile win_vs2022_x86_static_release --build=missing")
      Dir.chdir("build") do
        #run("cmake -DCMAKE_BUILD_TYPE=Release ..")
        #run("cmake --build .")
      end
    end
  else
    run("conan create .")
    Dir.chdir("test") do
      Dir.mkdir("build") if !Dir.exists?("build")
      PROJECT.run("conan install . --install-folder build")
      Dir.chdir("build") do
        PROJECT.run("cmake ..")
        PROJECT.run("cmake --build .")
      end
    end
  end
end

task :test => [:build] do
  start_task :test
  if Gem.win_platform?
    run("test_package/build/Release/roam_package_test.exe").details
    #run("test/build/bin/roam_test.exe").details.summary(true)
  end
end

task :default => [:test, :tag] do
  if (!PROJECT.read_only?)
    run("git integrate")
    run("git pull")
    run("git push")
    Raykit::Command.new("git --version").summary.details_on_failure
  end
  #puts `test_package/build/Release/roam_test`
  #run("git clean -dXf")
  puts "  completed in #{PROJECT.elapsed}"
end

task :tag => [:test] do
  start_task :tag
  if ENV["CI_SERVER"].nil?
    if GIT_DIRECTORY.has_tag "v#{VERSION}"
      puts "  git tag v#{VERSION} already exists"
    else
      puts "  git tag v#{VERSION} does not exist"
      if (!PROJECT.read_only?)
        run("git integrate")
        run("git tag -a v#{VERSION} -m\"version #{VERSION}\"")
        run("git push --tags")
      end
    end
  else
    puts "  CI_SERVER, skipping tag command"
  end
end

task :cmake do
  start_task :cmake
  run("git clean -dXf")
  run("conan install . --install-folder build --build=missing -pr=win_vs2022_x64_static_release")
  Dir.chdir("build") do
    run("cmake -A Win32 ..")
    run("cmake --build . --config Release")
  end
end

task :make do
  start_task :make
  run("conan remove roam/* --force").details
  roam_version = "0.0.3"
  if (`conan search roam/#{roam_version}`.include?("no packages matching"))
    puts "  roam/#{roam_version} recipe not found"
    Raykit::Git::Repository::make("https://gitlab.com/cpp-lib/roam.git", "v#{roam_version}", "rake build")
  else
    puts "  roam/#{roam_version} recipe found"
  end
end
