#pragma once

#include <string>
#include <any>
#include <vector>
#include <optional>
#include <sstream>
#include <set>

namespace roam{
  enum ValueType {
    VT_NONE = 0,
    VT_BOOL = 1,
    VT_INT32 = 2,
    VT_INT64 = 3,
    VT_UINT32 = 4,
    VT_UINT64 = 5,
    VT_FLOAT = 6,
    VT_DOUBLE = 7,
    VT_STRING = 8,
    VT_BYTES = 9,
    VT_BRANCH = 10
  };
  std::any get_default_value(ValueType vt);
}

namespace roam{
  struct Property {
    public:
      Property();
      Property(const char* name,ValueType type,std::any value);
      unsigned long long id;
      std::string name;
      ValueType type;
      std::any value;
      std::vector<std::any> values;
  };
  
}

namespace roam{
  struct Parameter {
    public:
      Parameter();
      Parameter(const char* name,ValueType type);
    public:
      std::string name;
      ValueType type;
      std::any value;
  };
  
}

namespace roam{
  struct Method {
    public:
       Method();
       Method(unsigned long long id,const char* name);
       Method(unsigned long long id,const char* name,Parameter p1);
       Method(unsigned long long id,const char* name,Parameter p1,Parameter p2);
       Method(unsigned long long id,const char* name,Parameter p1,Parameter p2,Parameter p3);
       Method(unsigned long long id,Parameter return_parameter,const char* name);
       Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1);
       Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2);
       Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2,Parameter p3);
    public:
      unsigned long long id;
      std::string name;
      std::string type;
      std::vector<Parameter> parameters;
      Parameter return_parameter;
      std::optional<Parameter> get_parameter(const char* name) const;
      std::any get_any_value(const char* name) const;
      template<typename T>
      T get_value(const char* name) const {
        return std::any_cast<T>(get_any_value(name));
      }
      void set_parameter(const char* name,const std::any& value);
      std::string to_s() const;
  };
}
namespace roam{
  struct Branch {
    public:
    Branch();
    Branch(const char* type);
    Branch(const char* type,const char* name);
    Branch(unsigned long long id,const char* type,const char* name);
    unsigned long long id;
    std::string name;
    std::string type;
    std::vector<Property> properties;
    std::vector<Method> methods;
    std::vector<Branch> children;
    std::optional<Property> get_property(const char* name) const;
    void set_property_value(const char* name,const std::any& value);
    std::optional<Method> get_method(const char* name) const;
    std::vector<Branch> descendants() const;
    std::vector<Branch> descendants_of_type(const char* type) const;
    std::vector<Branch> flatten() const;
    std::set<std::string> types() const;
  };
}

namespace roam{
  struct GetArgs{
    public:
      unsigned long long id;
      // a depth of 0 indicates that no descendant branches are collected
      // a depth of 1 indicates that direct children are to be collected
      // a depth of 2 indicates that children and grandchildren are to be collected
      unsigned int depth; 
      bool properties;
      bool methods;
      std::vector<std::string> exclude_types;
      std::vector<std::string> include_types;
      bool allow_partial_matches;
      bool is_type_match(const char* type) const;
  };
}

namespace roam{
  struct CollectArgs{
    public:
      unsigned long long id;
      // a depth of 0 indicates that no descendant branches are collected
      // a depth of 1 indicates that direct children are to be collected
      // a depth of 2 indicates that children and grandchildren are to be collected
      unsigned int depth; 
      bool properties;
      bool methods;
      std::vector<std::string> exclude_types;
      std::vector<std::string> include_types;
      bool allow_partial_matches;
      bool is_type_match(const char* type) const;
  };
}
namespace roam{
  class ITree{
    public:
      ITree();
      ITree(unsigned long long root_id,const char* name,const char* description);
      std::string name;
      std::string description;
      unsigned long long root_id;
      virtual std::optional<Branch> get(GetArgs args) const = 0;
      virtual std::vector<Property> set(std::vector<Property> properties) = 0;
      virtual Parameter invoke(const Method& method) = 0;
  };
}

namespace roam{
  class Tree : public ITree {
    public:
      Tree();
      Tree(Branch root);
      Branch root;
      virtual std::optional<Branch> get(GetArgs args) const override;;
      virtual std::vector<Property> set(std::vector<Property> properties) override;
      virtual Parameter invoke(const Method& method) override;
    private:
      std::optional<Branch> find_branch(unsigned long long id) const;
  };
}

std::ostream& operator<<(std::ostream& os, const roam::ValueType& vt);
std::ostream& operator<<(std::ostream& os,const std::pair<roam::ValueType,std::any>& v);
std::ostream& operator<<(std::ostream& os, const roam::Property& p);
std::ostream& operator<<(std::ostream& os, const roam::Parameter& p);
std::ostream& operator<<(std::ostream& os, const roam::Method& p);
std::ostream& operator<<(std::ostream& os, const roam::Branch& p);
std::ostream& operator<<(std::ostream& os, const std::pair<int,roam::Branch>& pair);