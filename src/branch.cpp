#include <iostream>
#include "roam/roam.h"

using namespace std;
using namespace roam;

Branch::Branch() : id(0), type("Branch"),name("0"){}
Branch::Branch(const char* type) : id(0),type(type),name("0"){}
Branch::Branch(const char* type,const char* name): id(0),type(type),name(name){}
Branch::Branch(unsigned long long id,const char* type,const char* name) : id(id),type(type),name(name){}
optional<Property> Branch::get_property(const char* name) const{
    for(auto& p : properties){
        if(p.name == name){
            return optional<Property>(p);
        }
    }
    return optional<Property>();
}
void Branch::set_property_value(const char* name,const any& value){
    for(auto& p : properties){
        if(p.name == name){
            p.value = value;
        }
    }
}
optional<Method> Branch::get_method(const char* name) const{
    for(auto& m : methods){
        if(m.name == name){
            return optional<Method>(m);
        }
    }
    return optional<Method>();
}
vector<Branch> Branch::descendants() const{
    vector<Branch> results;
    for(auto& child : children){
        results.push_back(child);
        for(auto& descendant : child.descendants()){
            results.push_back(descendant);
        }
    }
    return results;
}
vector<Branch> Branch::descendants_of_type(const char* type) const{
    vector<Branch> results;
    for(auto& child : descendants()){
        if(child.type == type){
            results.push_back(child);
        }
    }
    return results;
}
vector<Branch> Branch::flatten() const{
    vector<Branch> results;
    results.push_back(*this);
    for(auto& d : descendants()){
        results.push_back(d);
    }
    return results;
}
set<string> Branch::types() const{
    set<string> types;
    for(auto& child : children){
        types.insert(child.type);
        for(auto& t : child.types()){
            types.insert(t);
        }
    }
    return types;
}