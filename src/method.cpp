#include <iostream>
#include "roam/roam.h"

using namespace std;
using namespace roam;

Method::Method() : id(0),name("") { return_parameter = Parameter("void",roam::ValueType::VT_NONE);}
Method::Method(unsigned long long id,const char* name):id(id),name(name){}
Method::Method(unsigned long long id,const char* name,Parameter p1) : Method(id,name) {
    parameters.push_back(p1);
}
Method::Method(unsigned long long id,const char* name,Parameter p1,Parameter p2) : Method(id,name,p1){
    parameters.push_back(p2);
}
Method::Method(unsigned long long id,const char* name,Parameter p1,Parameter p2,Parameter p3) : Method(id,name,p1,p2){
    parameters.push_back(p3);
}
Method::Method(unsigned long long id,Parameter return_param,const char* name) : Method(id,name){
    return_parameter = return_param;
}
Method::Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1) 
 : Method(id,return_parameter,name){
    parameters.push_back(p1);
}
Method::Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2) 
 : Method(id,return_parameter,name,p1){
    parameters.push_back(p2);
}
Method::Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2,Parameter p3) 
 : Method(id,return_parameter,name,p1,p2){
    parameters.push_back(p3);
}
optional<Parameter> Method::get_parameter(const char* name) const{
    for(auto& p : parameters){
        if(p.name == name){
            return optional<Parameter>(p);
        }
    }
    return optional<Parameter>();
}
void Method::set_parameter(const char* name,const any& value) {
    for(auto& p : parameters){
        if(p.name == name){
            p.value = value;
        }
    }
}
std::string Method::to_s() const{
    ostringstream os;
    os << *this;
    return os.str();
}
std::any Method::get_any_value(const char* name) const {
    std::optional<Parameter> opt = get_parameter(name);
    if(opt.has_value()){
        return opt.value().value;
    }
    else{
        std::ostringstream os;
        os << "parameter '" << name << "' was not found for method " << to_s();
        throw std::runtime_error(os.str().c_str());
    }
}