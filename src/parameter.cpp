#include <iostream>
#include "roam/roam.h"

using namespace std;
using namespace roam;

Parameter::Parameter() : type(ValueType::VT_NONE) { value=get_default_value(ValueType::VT_NONE);}
Parameter::Parameter(const char* name,ValueType type):name(name),type(type){value=get_default_value(type);}
