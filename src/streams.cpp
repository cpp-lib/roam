#include "roam/roam.h"
#include <iomanip>

using namespace std;
using namespace roam;
ostream& operator<<(ostream& os, const ValueType& vt)
{
      string name("?");
      switch (vt) {
        case VT_NONE:
          name = "void";
          break;
        case VT_BOOL:
          name = "bool";
          break;
        case VT_INT32:
          name = "int32";
          break;
        case VT_INT64:
          name = "int64";
          break;
        case VT_UINT32:
          name = "uint32";
          break;
        case VT_UINT64:
          name = "uint64";
          break;
        case VT_FLOAT:
          name = "float";
          break;
        case VT_DOUBLE:
          name = "double";
          break;
        case VT_STRING:
          name = "string";
          break;
        case VT_BYTES:
          name = "bytes";
          break;
        case VT_BRANCH:
          name = "Branch";
          break;
      }
      return os << name;
}
ostream& operator<<(ostream& os,const pair<ValueType,any>& pair){

      switch (pair.first) {
        case VT_NONE:
          return os << "";
          break;
        case VT_BOOL:
          return os << any_cast<bool>(pair.second);
          break;
        case VT_INT32:
          return os << any_cast<long>(pair.second);
          break;
        case VT_INT64:
          return os << any_cast<long long>(pair.second);
          break;
        case VT_UINT32:
          return os << any_cast<uint32_t>(pair.second);
          break;
        case VT_UINT64:
          return os << any_cast<uint64_t>(pair.second);
          break;
        case VT_FLOAT:
          return os << any_cast<float>(pair.second);
          break;
        case VT_DOUBLE:
          return os << any_cast<double>(pair.second);
          break;
        case VT_STRING:
          return os <<any_cast<string>(pair.second);
          break;
        case VT_BYTES:
          return os << "";
          break;
        case VT_BRANCH:
          return os << "";
          break;
      }
      return os << "";
}
ostream& operator<<(ostream& os, const Property& p)
{ return os << p.type << " " << p.name << " " << make_pair(p.type,p.value);}
ostream& operator<<(ostream& os, const Parameter& p)
{ return os << p.type << " " << p.name << " " << make_pair(p.type,p.value);}
ostream& operator<<(ostream& os, const Method& p){
  ostringstream returns;
  if(p.return_parameter.type == ValueType::VT_NONE){//}.return_parameters.size() == 0){
    returns << "void";
  }
  else{
    returns << p.return_parameter.type;
    /*
    if(p.return_parameters.size() == 1){
      returns << p.return_parameters[0].type;
    }
    else{
      returns << "[";
      int index = 0;
      for(auto& rp : p.return_parameters){
        if(index > 0) { returns << ",";}
        returns << rp;
        ++index;
      }
      returns << "]";
    }*/
  }
  ostringstream params;
  params << "(";
  int pindex =0;
  for(auto& param : p.parameters){
    if(pindex > 0){ params << ",";}
    params << param.type << " " << param.name;
    ++pindex;
  }
  params << ")";
  return os << returns.str().c_str() << " " << p.name << params.str();
}
ostream& operator<<(ostream& os, const roam::Branch& b){
    return os << make_pair(0,b);
}
ostream& operator<<(ostream& os, const pair<int,roam::Branch>& pair){
    string indent = "";
    if(pair.first > 0){
        ostringstream iss;
        iss << setw(pair.first) << " ";
        indent = iss.str();
    }

    const Branch& b = pair.second;
    os << indent << "Branch " << b.name << " (" << b.type << "," << b.id << ")" << endl;
    if(!b.properties.empty()){
      os << indent << "  " << b.properties.size() << " properties" << endl;
      for(auto& p : b.properties){
          os << indent << "    " << p << endl;
      }
    }
    if(!b.methods.empty()){
      os << indent << "  " << b.methods.size() << " methods" << endl;
      for(auto& m : b.methods){
          os << indent << "    " << m << endl;
      }
    }
    if(!b.children.empty()) {
      os << indent << "  " << b.children.size() << " children" << endl;
      for(auto& c : b.children){
          os << make_pair(pair.first+4,c);
      }
    }
    return os;
}