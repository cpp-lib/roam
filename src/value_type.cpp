#include "roam/roam.h"

//using namespace std;
//using namespace roam;

std::any roam::get_default_value(roam::ValueType vt){
    std::any result;
    switch(vt){
        case VT_BOOL:
            return false;
        case VT_INT32:
            return (int)0;
        case VT_INT64:
            return (long long)0;
        case VT_UINT32:
            return (unsigned int)0;
        case VT_UINT64:
            return (unsigned long long)0;
        case VT_FLOAT:
            return 0.0f;
        case VT_DOUBLE:
            return 0.0;
        case VT_STRING:
            return std::string("");
    }
    return result;
}
 